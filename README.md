# Happy numbers
This repository contains a script for check happy numbers from 1 to 1000.

## What is an happy number
Starting with any positive integer, replace the number by the sum of the squares of  its digits, and repeat the process until the number equals 1, or it loops endlessly in a cycle which does  not include 1.

### Example
Let's take the number 49 as example

`49 => 4*4 + 9*9 = 97`

`97 => 9*9 + 7*7 = 130`

`130 => 1*1 + 3*3 + 0*0 = 10`

`10 => 1*1 + 0*0 = 1`

So 49 is an happy number, but also 130, 10 and so on.

## What is an unhappy number
An unhappy number is a number that is not happy!

### Requirements
This script is tested with PHP < 5.3

### Installation
Simply clone the repository into your document root