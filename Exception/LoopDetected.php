<?php

class LoopDetected extends Exception
{
    public function __construct($msg = "Number already checked, found loop!")
    {
        parent::__construct($msg);
    }
}
