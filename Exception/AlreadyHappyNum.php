<?php

class AlreadyHappyNum extends Exception
{
    public function __construct($msg = "Number is already an happy number!")
    {
        parent::__construct($msg);
    }
}
