<?php

class AlreadyUnhappyNum extends Exception
{
    public function __construct($msg = "Number is already an unhappy number!")
    {
        parent::__construct($msg);
    }
}
