<?php

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Exception' . DIRECTORY_SEPARATOR . 'AlreadyHappyNum.php';
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Exception' . DIRECTORY_SEPARATOR . 'AlreadyUnhappyNum.php';
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . 'Exception' . DIRECTORY_SEPARATOR . 'LoopDetected.php';

class HappyNumbers
{

    protected $happyNumbers = array();
    protected $unhappyNumbers = array();
    protected $currentVisitedNumbers = array();

    /**
     * Get sum of digits squares
     *
     * @param int $num
     * @return int
     */
    protected function getSumOfDigitsSquares($num)
    {
        $numString = (string)$num;
        $total = 0;
        for ($i = 0; $i < strlen($numString); $i++) {
            $total += pow($numString[$i], 2);
        }
        return $total;
    }

    /**
     * @param int $num
     * @throws AlreadyHappyNum
     * @throws AlreadyUnhappyNum
     * @throws LoopDetected
     */
    protected function checkResult($num)
    {
        if (in_array($num, $this->currentVisitedNumbers)) {
            throw new LoopDetected();
        }
        if (in_array($num, $this->happyNumbers)) {
            throw new AlreadyHappyNum();
        }
        if (in_array($num, $this->unhappyNumbers)) {
            throw new AlreadyUnhappyNum();
        }
    }

    public function run()
    {
        $happyNumberCheck = 1;
        $this->happyNumbers[] = $happyNumberCheck;
        for ($i = 1; $i <= 1000; $i++) {
            try {
                $result = $i;
                $this->currentVisitedNumbers = array();
                while ($result != $happyNumberCheck) {
                    $this->checkResult($result);
                    $this->currentVisitedNumbers[] = $result;
                    $result = $this->getSumOfDigitsSquares($result);
                }
                $this->happyNumbers = array_merge($this->happyNumbers, $this->currentVisitedNumbers);
            } catch (AlreadyHappyNum $e) {
                $this->happyNumbers = array_merge($this->happyNumbers, $this->currentVisitedNumbers);
            } catch (LoopDetected $e) {
                $this->unhappyNumbers = array_merge($this->unhappyNumbers, $this->currentVisitedNumbers);
            } catch (AlreadyUnhappyNum $e) {
                $this->unhappyNumbers = array_merge($this->unhappyNumbers, $this->currentVisitedNumbers);
            }
        }
        sort($this->happyNumbers);
        sort($this->unhappyNumbers);

        echo sprintf("%d happy numbers founded:", count($this->happyNumbers));
        echo "<pre>" . implode(', ', $this->happyNumbers) . "</pre>";
        echo sprintf("%d unhappy numbers founded:", count($this->unhappyNumbers));
        echo "<pre>" . implode(', ', $this->unhappyNumbers) . "</pre>";
    }
}

$processor = new HappyNumbers();
$processor->run();